# Project 7: Adding authentication and user interface to brevet time calculator service

## What is in this repository

Utilized previous projects to create a user authentication system for access certain pages/resources.

Login using the Login button on top

Register as a user using the login button on the top


Click the display buttons after being logged in to access its view

Enter /secret webpage into address for another log in required access


Api resources and set up was redone, some/a lot of previous code does not work properly yet.

Only password/user name authentication is set up. Id's and tokens are used for authentication but

require a username/pass log in


There is currently no in app way to reset database due to oversight/lack of time. Any 4 string or 

character + number usernames should avoid any problems


## Author

Isaac Perks: iperks@uoregon.edu

Forked from: UOCISS 322 proj7-auth-ux
