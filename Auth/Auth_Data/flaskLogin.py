"""
Flask-Login example
===================
This is a small application that provides a trivial demonstration of
Flask-Login, including remember me functionality.

:copyright: (C) 2011 by Matthew Frazier.
:license: MIT/X11, see LICENSE for more details.
:source: https://gist.github.com/robbintt/32074560e52e46788237
"""
from flask import Flask, request, render_template, redirect, url_for, flash, Blueprint
import requests
import json
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from .password import *
from .testToken import *

# your user class 
class User(UserMixin):
    def __init__(self, id, name, active=True):
        self.name = name
        self.id = id
        self.active = active
        self.is_authenticated


class register_form(FlaskForm):
    uservalue = StringField('uservalue')
    passwordvalue = PasswordField('passwordvalue')

    def is_active(self):
        return self.active

# note that the ID returned must be unicode

#USER_NAMES = dict((u.name, u) for u in USERS.values())

app = Flask(__name__)
client = 0
user_set = 0
api = 0
SECRET_KEY = "yeah, not actually a secret"
DEBUG = True

app.config.from_object(__name__)

# step 1 in slides
login_manager = LoginManager()
login_manager.init_app(app)

# step 6 in the slides
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

def api_set(api_init, api_app):
    global client
    client = api_init
    global api
    api = api_app


def db_set(db_init):
    global user_set
    user_set = db_init


def api_db():
    global user_set
    global client
    client.db_set(user_set)

# step 2 in slides 
# step 2 in slides
@login_manager.user_loader
def load_user(id):
    search = {'password': id}
    return requests.get(api.url_for(client.b_Read, search=search, _external=True, _method='GET'))#client.b_Read.get(client.b_Read, search)


log_reroute = Blueprint('log_reroute', __name__)
reauth_reroute = Blueprint('reauth_reroute', __name__)
logout_reroute = Blueprint('logout_reroute', __name__)
secret_reroute = Blueprint('secret_reroute', __name__)
register_reroute = Blueprint("register_reroute", __name__)
#@app.route("/")
#def index():
  #  return render_template("index.html")
class userToken():
    def get(self):
        userpass = request.args.get('pass')
        hval = hash_password(userpass)
        search = {'password': hval}
        print (hVal)
        if verify_password(userPas, hVal):
            token = {'id': generate_auth_token(expiration=600)}
            client.b_Save.get(client.b_Save, search, token)
            return jsonify({'token': token, 'duration': 600})
        else:
            print ("Failure")


@app.route('/api/token', methods=['GET'])
def token__auth():
    userResponse = userToken.get(request.form["password"])
    if userResponse is not "Failure":
        return userResponse
    else:
        abort(401)


@register_reroute.route("/register", methods=["POST", "GET"])
@app.route("/register")
def register_user():
    registration_form = register_form()
    if registration_form.validate_on_submit():
        api_db()
        user_password = str(registration_form.passwordvalue).encode('utf-8')
        user_password = hash_password(user_password)
        id = str(generate_auth_token(expiration=600)).replace('\'', '\"')
        user_name = str(registration_form.uservalue.data)
        parse_data = {'id': id, 'name': user_name, 'active': True, 'password': user_password}
        requests.post(api.url_for(client.b_Create, data=parse_data, _external=True, _method='POST'))#client.b_Create.post(client.b_Create, parse_data)
        return redirect(request.args.get("next") or url_for("index"))
    return render_template('auth_template.html', form=registration_form)


@secret_reroute.route("/secret", methods=['GET'])
@app.route("/secret")
@fresh_login_required
def secret():
    return render_template("secret.html")

# step 3 in slides
# This is one way. Using WTForms is another way.
@log_reroute.route("/login", methods=["GET","POST"])
@app.route("/login", methods=["GET", "POST"])
def log_user():
    user_form = register_form()
    api_db()
    if user_form.validate_on_submit():#request.method == "POST" and "username" in request.form and "password" in request.form:
        username = str(user_form.uservalue.data)
        password = str(user_form.passwordvalue).encode('utf-8')
        #password = hash_password(password)
        search = {'name': username}
        user_search = requests.get(api.url_for(client.b_Read, search=search, _external=True, _method='GET'))
        user_search = user_search.json()
        #client.b_Read.get(client.b_Read, search)
        user_search = json.loads(user_search)
        if user_search is not None:
            name = user_search['name']
            id = user_search['id']
            if verify_password(password, user_search['password']):
                user_search = User(id=id, name=name, active=True)
                #remember = request.form.get("remember", "no") == "yes"
                if login_user(user_search):
                    flash("Logged in!")
                    return redirect(request.args.get("next") or url_for("index"))
                else:
                    flash("Sorry, but you could not log in.")
            else:
                return str(password) +" "+ str(user_search['password'])
        else:
            flash(u"Invalid username.")
    return render_template('login.html', form=user_form)

# step 5 in slides
@reauth_reroute.route("/reauth", methods=["GET","POST"])
@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash(u"Reauthenticated.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("index"))

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
