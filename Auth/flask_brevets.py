"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask
from flask import request, url_for, jsonify
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
import requests
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import os
import json
from webapi import api as b_api
from Auth_Data import flaskLogin as flask_login
from Auth_Data.flaskLogin import log_reroute, logout_reroute, secret_reroute, reauth_reroute, register_reroute
from flask_restful import Api
import csv

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
control_list = client.controls.samples
b_api.db_set(control_list)
flask_login.api_set(b_api, api)
flask_login.db_set(client.users.data)
doc_count = b_api.client.count_documents({}) #Sets number of documents in db. Updated whenever a new doc is created
from webapi.api import b_Read, b_Create

login_manager = LoginManager()
login_manager.login_view = "log_reroute.login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth_reroute.reauth"
login_manager.init_app(app)
api.add_resource(b_Read,'/api/read')
api.add_resource(b_Create,'/api/register')

class User(UserMixin):
    def __init__(self, id, name, active=True):
        self.name = name
        self.id = id
        self.active = active
        self.is_authenticated

@login_manager.user_loader
def load_user(user_id):
    search = {'id': user_id}
    user_search = requests.get(api.url_for(b_api.b_Read, search=search, _external=True, _method='GET'))
    user_search = user_search.json()
    user_search = json.loads(user_search)
    name = user_search['name']
    id = user_search['id']
    user_search = User(id=id, name=name, active=True)
    return user_search

##############
#Cut/Paste Functions
##############
def csv_parse(file_name, fieldnames, input, *args):
    result = ""
    with open(file_name, "w") as close_text:
        data_writer = csv.DictWriter(close_text, fieldnames=fieldnames)
        data_writer.writeheader()
        if args:
            top = args[1]
            for x in range(doc_count, limit=top):
                search = {'id': x}
                if str(b_api.b_Read.get(b_api.b_Read, search, input)):
                    data_writer.writerow(b_api.b_Read.get(b_api.b_Read, search, input))
        else:
            for x in range(doc_count):
                search = {'id': x}
                if str(b_api.b_Read.get(b_api.b_Read, search, input)):
                    data_writer.writerow(b_api.b_Read.get(b_api.b_Read, search, input))

    with open(file_name, 'r') as read_file:
        data_reader = csv.reader(read_file)
        for row in data_reader:
            result += str(row) + "\n "
        read_file.close
    os.remove(file_name)
    return result


def json_parse(top, input):
    data_list = {}
    if top:
        if doc_count:
            for x in range(doc_count, limit=top):
                search = {'id': x}
                data_list[x] = b_api.b_Read.get(b_api.b_Read, search, input)
                app.logger.debug(str(x))
            return data_list
        else:
            return "fail"
    else:
        if doc_count:
            for x in range(doc_count):
                search = {'id': x}
                data_list[x] = b_api.b_Read.get(b_api.b_Read, search, input)
                app.logger.debug(str(x))
            return data_list
        else:
            return "fail"
###
# Pages
###
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    flask.session['data_entries'] = 0
    return flask.render_template('calc.html')


@app.route("/_display_values", methods=['GET'])
@app.route("/listAll", methods=['GET'])
@app.route("/listAll.json")
@login_required
def list_all():
    data_list = ""
    if doc_count:
        for x in range(0, doc_count):
            search = {'id': x}
            parse_url = requests.get(api.url_for(b_Read, search=search, _external=True, _method='GET'))
            data_list += parse_url.text   #str(b_api.b_Read.get(b_api.b_Read, search)) + "\n "
        return flask.render_template('display.html', db_data=data_list)
    else:
        return flask.render_template('calc.html')


@app.route("/listAll.csv")
def csv_list():
    fieldnames = ['close_time', 'id', 'open_time', 'km']
    top = request.args.get('top', type=int)
    input = {'_id': 0}
    file_name = 'open_file.csv'
    if top:
        if doc_count:
            result = csv_parse(file_name, fieldnames, input, top)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')
    else:
        if doc_count:
            result = csv_parse(file_name, fieldnames, input)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')


@app.route("/listOpenOnly.csv")
def csv_open():
    fieldnames = ['open_time']
    top = request.args.get('top', type=int)
    input = {'id': 0, 'close_time': 0, '_id': 0, 'km': 0}
    file_name = 'open_file.csv'
    if top:
        if doc_count:
            result = csv_parse(file_name, fieldnames, input, top)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')
    else:
        if doc_count:
            result = csv_parse(file_name, fieldnames, input)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')


@app.route("/listCloseOnly.csv")
def csv_close():
    fieldnames = ['close_time']
    top = request.args.get('top', type=int)
    input = {'id': 0, 'open_time': 0, '_id': 0, 'km': 0}
    file_name = 'close_file.csv'
    if top:
        if doc_count:
            result = csv_parse(file_name, fieldnames, input, top)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')
    else:
        if doc_count:
            result = csv_parse(file_name, fieldnames, input)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')


@app.route("/listOpenOnly")
@app.route("/listOpenOnly.json")
def open_only():
    top = request.args.get('top', type=int)
    input = {'id': 0, 'close_time': 0, '_id': 0, 'km': 0}
    parse = json_parse(top, input)
    if not parse == "fail":
        return flask.render_template('display.html', parse)
    else:
        return flask.render_template('calc.html')

@app.route("/listCloseOnly")
@app.route("/listCloseOnly.json")
def close_only():
    top = request.args.get('top', type=int)
    input = {'id': 0, 'open_time': 0, '_id': 0, 'km': 0}
    parse = json_parse(top, input)
    if not parse == "fail":
        return flask.render_template('display.html', parse)
    else:
        return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
#Login Handling
#
###############

app.register_blueprint(log_reroute)
app.register_blueprint(secret_reroute)
app.register_blueprint(reauth_reroute)
app.register_blueprint(logout_reroute)
app.register_blueprint(register_reroute)
################
#
# AJAX DB handlers
#   for entering/returning data for mongoDB
#################
@app.route("/_update_values", methods=['POST', 'PUT'])
def _update_values():
    #Responds to html submission request
    #Data gathered from fields
    #Enters request into database
    app.logger.debug("Got a AJAX request")
    global doc_count
    km = str(request.form.get('km', type = int))
    open_time = request.form.get('brevet_open', type = str)
    close_time = request.form.get('brevet_close', type = str)
    parse_data = {'id': doc_count, 'km': km, 'open_time': open_time, 'close_time': close_time}
    result = str(parse_data)
    if open_time == "Proper Time" or open_time == "Positive Integer" or close_time == "Proper Time" or close_time =="Positive Integer":
        return flask.jsonify(result="improper fields in data, please properly fill them in.")
    if open_time and close_time:
        if control_list.find_one(parse_data) == parse_data:
            result = "Data already exists. Please enter new data"
        else:
            b_api.b_Create.post(b_api.b_Create, parse_data)
            app.logger.debug("got a db update: " + str(control_list.find_one(parse_data)))
            search = {id: doc_count}
            if b_api.b_Read.get(b_api.b_Read, search):
                flask.session['data_entries'] =+ 1
                doc_count = b_api.client.count_documents({})
                result = "success"
            else:
                result = "fail"
    return result



###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
        
   # Calculates open/close times from miles, using rules
   # described at https://rusa.org/octime_alg.html.
   # Expects one URL-encoded argument, the number of miles.
    
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', type = float)
    total_distance = request.args.get('brevet_distance', type = int)
    start_time = request.args.get('timestamp', type = str)
    start_time = arrow.get(start_time)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, total_distance, start_time.isoformat())
    close_time = acp_times.close_time(km, total_distance, start_time.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug = True)