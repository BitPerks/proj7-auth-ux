# Laptop Service

import flask
from flask import jsonify, request
from flask_restful import Resource, Api, reqparse
from pymongo import MongoClient
import os
import ast
import config
import json

# Instantiate the app
app = flask.Flask(__name__)
web_api = Api(app)
client = "empty"


def db_set(db_init):
    global client
    client = db_init


class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }


class b_Sort(Resource):
    def get(self, req):
        pass


class b_Create(Resource):
    def post(self):
        args = request.args
        data = ast.literal_eval(args.get('data'))
        if data:
            client.insert_one(data)

class b_Delete(Resource):
    def get(self):
        #add data delete?
        return{"Resource b_delete was called"}

class b_Read(Resource):
    def get(self):
        args = request.args
        search = ast.literal_eval(args.get('search'))
        if args.get('limit') is None:
            data = client.find_one(search, {'_id': 0})
            if data:
                return json.dumps(data)
            else:
                return str(client.find({}))
        else:
            b_data_id = ast.literal_eval(args.get('limit'))
            data = client.find_one(search, b_data_id)
            if data:
                return data
            else:
                return 0

class b_Save(Resource):
    def get(self):
        args = request.args
        search = ast.literal_eval(args.get('search'))
        token = ast.literal_eval(args.get('token'))
        client.update_one(search, token)
        return "Finished"

# Create routes
# Another way, without decorators
#Call these in brevets main code to use api
#api.add_resource(Laptop, '/')
#api.add_resource(b_create,'/_create')
#api.add_resource(b_delete,'/_delete'))

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
